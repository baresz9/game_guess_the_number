import random
from termcolor import colored

#GuessGameHUN
def LuckyBoyOrGirl():
    print('Hello Játékos, hogy hívnak?')
    nameOfPlayer = input()
    print('Kedves ' + nameOfPlayer + ' , találd ki azt a számot amire gondoltam. Ez a szám 1 és 20 között van!')
    randomNumber = random.randint(1,20)
    
    for guessTries in range(1,7):
        print('Mi a(z) ' + str(guessTries) + '. tipped?')
        try:
            guess = input()
            if int(guess) > randomNumber:
                print('Ennél kisebb számra gondoltam')
            elif int(guess) < randomNumber:
                print('Ennél nagyobb számra gondoltam')
            else:
                break # tehát ugorjon ki a for ciklusból hiszen nem tud/ kell többször próbálkoznia
            if guessTries == 5:
                print(colored('\nVigyázz, már csak egy próbálkozásod van!', 'red'))    
        except (ValueError, NameError, TypeError):
            print('Kérlek számot adj be')
        
    if guess == randomNumber:
        print('Gratulálok sikeresen kitaláltad azt a számot amire gondoltam ' + nameOfPlayer + ' !')
    else:
        print('\nElfogyott az összes próbálkozásod, sajnos ez most nem jött össze. A szám amire gondoltam a ' + randomNumber + ' volt :(')
        print('Kérlek indíts el újból a játékot!')
LuckyBoyOrGirl()